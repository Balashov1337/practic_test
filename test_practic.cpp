﻿#include <iostream>
#include "math.h"
#define HAVE_STRUCT_TIMESPEC
#include <pthread.h>
#include <climits>         



#define ERROR_CREATE_THREAD -11
struct list
{
	int field; // поле данных
	struct list *next; // указатель на следующий элемент
	struct list *prev; // указатель на предыдущий элемент
} ;

struct list * init(int a)  // а- значение первого узла
{
	struct list *lst;
	// выделение памяти под корень списка
	lst = (struct list*)malloc(sizeof(struct list));
	lst->field = a;
	lst->next = NULL; // указатель на следующий узел
	lst->prev = NULL; // указатель на предыдущий узел
	return(lst);
}

struct list * addelem(list *lst, int number)
{
	struct list *temp, *p;
	temp = (struct list*)malloc(sizeof(list));
	p = lst->next; // сохранение указателя на следующий узел
	lst->next = temp; // предыдущий узел указывает на создаваемый
	temp->field = number; // сохранение поля данных добавляемого узла
	temp->next = p; // созданный узел указывает на следующий узел
	temp->prev = lst; // созданный узел указывает на предыдущий узел
	if (p != NULL)
		p->prev = temp;
	return(temp);
}

struct list * deletelem(list *lst)
{
	struct list *prev, *next;
	prev = lst->prev; // узел, предшествующий lst
	next = lst->next; // узел, следующий за lst
	if (prev != NULL)
		prev->next = lst->next; // переставляем указатель
	if (next != NULL)
		next->prev = lst->prev; // переставляем указатель
	free(lst); // освобождаем память удаляемого элемента
	return(prev);
}

void listprint(list *lst)
{
	struct list *p;
	p = lst;
	do {
		int value = p->field;
		size_t num_zeroes = 0;

		for (size_t i = 0; i < CHAR_BIT * sizeof value; ++i)
		{
			if ((value & (1 << i)) == 0)
				++num_zeroes;
		}
		printf("%d ", p->field);
		p = p->next; // переход к следующему узлу
	} while (p != NULL); // условие окончания обхода
}

void* listprint_thread(void* args)
{	printf("\nTHREAD -> WORK\n");
	list *p = (list*) args;
	int summary = 0 ;
	do {
		int value = p->field;
		size_t num_zeroes = 0;

		for (size_t i = 0; i < CHAR_BIT * sizeof value; ++i)
		{
			if ((value & (1 << i)) == 0)
				++num_zeroes;
		}
		summary += num_zeroes;
		printf("element - %d \n",p->field);
		p = p->next; // переход к следующему узлу
	//	deletelem(p->prev);
	} while (p->next != NULL); // условие окончания обхода
	printf("SUMMARY ZEROS = %d", summary);
	printf("\nstop THREAD -> WORK\n");
	return 0;
}

void* listprintr_thread(void* args)
{	
	printf("\nTHREAD <- WORK\n");
	list *p = (list*) args;
       	while (p->next != NULL)
       		p = p->next;  // переход к концу списка
	int summary = 0;
	do {
		int value = p->field;
		size_t num_zeroes = 0;
		for (size_t i = 0; i < CHAR_BIT * sizeof value; ++i)
		{
			if ((value & (1 << i)) != 0)
			++num_zeroes;
		}
		summary += num_zeroes;
		printf("element revers - %d \n",p->field);
		p = p->prev; // переход к следующему узл
	//	deletelem(p->next);
	} while (p->prev != NULL); // условие окончания обхода
	printf("SUMMARY NON ZEROS = %d",summary);
	printf("\nstopTHREAD <- WORK\n");
	return 0;
	}
	
int main()
{
	list *head, *cur;
	int num;
	pthread_t thread;
	int status;
	int status_addr;
	// Создаем список из 100 элементов
	num = rand();
	head = init(num);
	cur = head;
	for (int i = 0; i < 99; i++) {
		cur = addelem(cur, rand());
	}
	/////////////////////////////////////////////
	pthread_create(&thread, NULL, listprint_thread, head);
	pthread_create(&thread, NULL, listprintr_thread, head);
	printf("\n================MAIN THREAD===============\n");
	pthread_join(thread, NULL);
	return 0;

}
